package supinfo.com.supcooking.dao;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@DisplayName("Testing CategoryDAO Test class")
public class CategoryDAOTest {
    @Test
    @DisplayName("Should be able to create DAO")
    void daoCreation() {
        CategoryDAO dao = new CategoryDAO();
        assertNotNull(dao);
        assertNotNull(dao.getEntityManager());
    }
}
