package supinfo.com.supcooking.dao;

import supinfo.com.supcooking.Exeception.*;
import supinfo.com.supcooking.bean.Ingredient;
import supinfo.com.supcooking.bean.IsIn;
import supinfo.com.supcooking.bean.Recipe;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

public class IsInDAO {
    private Logger log = Logger.getLogger(this.getClass().getName());
    private static String persistenceUnitName = "SupCooking";
    private static EntityManager entityManager =  Persistence.createEntityManagerFactory(persistenceUnitName).createEntityManager();
    private static RecipeDAO recipeDAO = new RecipeDAO();
    private static IngredientDAO ingredientDAO = new IngredientDAO();

    public void add(int recipeID, String ingredientName, int ingredientQuantity, String quantityUnit) throws Exception {
        if (recipeID == 0 ||
                ingredientName.equals("")) {
            throw new NotNullConstraintException();
        }
        if (ingredientQuantity == 0) {
            ingredientQuantity ++;
        }
        if (quantityUnit.equals("")) {
            quantityUnit = "u";
        }
        Recipe recipe = recipeDAO.get(recipeID);
        if (recipe == null || recipe.equals(new Recipe())) {
            throw new FKRecipeException();
        }
        Ingredient ingredient = ingredientDAO.get(ingredientName).get(0);
        if (ingredient == null || ingredient.equals(new Ingredient())) {
            throw new FKIngredientException();
        }
        try {
            entityManager.getTransaction().begin();
            IsIn isIn = new IsIn();
            isIn.setIngredientByIngredient(ingredient);
            isIn.setQuantity(ingredientQuantity);
            isIn.setUnit(quantityUnit);
            isIn.setRecipeByRecipe(recipe);
            entityManager.persist(isIn);
            entityManager.flush();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            log.log(Level.SEVERE, e.getMessage());
            throw e;
        }

    }

    public List<IsIn> get(Recipe recipe) {
        try {
            return(List<IsIn>) entityManager.createQuery("select i from IsIn i where i.recipeByRecipe=:recipe")
                    .setParameter("recipe",recipe).getResultList();
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return null;
        }
    }

    public List<Ingredient> getIngredients(Recipe recipe) {
        try {
            return (List<Ingredient>) entityManager.createQuery("select i.ingredientByIngredient from IsIn i where i.recipeByRecipe=:recipe")
                    .setParameter("recipe",recipe).getResultList();
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return null;
        }
    }

    private boolean delete(IsIn isIn) {
        try {
            /*int res = entityManager.createQuery("DELETE FROM IsIn where IsIn.id = :isIn").setParameter("isIn", isIn.getIdIsIn()).executeUpdate();*/
            entityManager.getTransaction().begin();
            entityManager.remove(isIn);
            entityManager.flush();
            entityManager.getTransaction().commit();
            return true;
        } catch (Exception e) {
            log.log(Level.WARNING, "Could not delete isIn.\nError: " + e.getStackTrace());
            return false;
        }
    }

    boolean deleteRecipeLink(Recipe recipe) {
        try {
            List<IsIn> isIns = this.get(recipe);
            boolean ok;
            for (IsIn isIn: isIns) {
                ok = this.delete(isIn);
                if (!ok) {
                    return false;
                }
            }
        } catch (Exception e) {
            log.log(Level.SEVERE,"Error while deleting links.");
            return false;
        }
        return false;

    }

    public void Update(IsIn r, IsIn nr){
        entityManager.getTransaction().begin();
        try {
            String o = r.getUnit();
            String n = nr.getUnit();
            if (!(n==null || (o.equals(n)))) {
                r.setUnit(n);
            }
            int io=r.getQuantity();
            int in=r.getQuantity();
            if (!(in == 0 || (io == in))) {
                r.setQuantity(in);
            }
            Recipe ro=r.getRecipeByRecipe();
            Recipe rn=r.getRecipeByRecipe();
            if (!(rn == null || (ro.equals(rn)))) {
                r.setRecipeByRecipe(rn);
            }
            Ingredient ino=r.getIngredientByIngredient();
            Ingredient inn=r.getIngredientByIngredient();
            if (!(inn == null || (ino.equals(inn)))) {
                r.setIngredientByIngredient(inn);
            }
            entityManager.merge(r);
            entityManager.flush();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            entityManager.getTransaction().rollback();
        }
    }

}
