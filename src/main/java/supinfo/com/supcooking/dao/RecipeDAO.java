package supinfo.com.supcooking.dao;

import supinfo.com.supcooking.Exeception.*;
import supinfo.com.supcooking.bean.Category;
import supinfo.com.supcooking.bean.Recipe;
import supinfo.com.supcooking.bean.User;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.regex.Pattern;

public class RecipeDAO {
    private Logger log = Logger.getLogger(this.getClass().getName());
    private static String persistenceUnitName = "SupCooking";
    private static EntityManager entityManager =  Persistence.createEntityManagerFactory(persistenceUnitName).createEntityManager();
    private static String recipeRegex = "^[a-zA-Z]+[a-zA-Z_ ]*[a-zA-Z]+";
    private static Pattern recipeName = Pattern.compile(recipeRegex);
    private static UserDAO userDAO = new UserDAO();
    private static CategoryDAO categoryDAO = new CategoryDAO();
    private static IsInDAO isInDAO = new IsInDAO();
    private static RankDAO rankDAO = new RankDAO();
    private Category cat;
    private User usr;

    public Recipe add(String name, String category, String description, int numberOfProducts, String publishedBy,
                   String picture, int hardness, int prep, int cooking) throws Exception {
        if (name == null || name.equals("") ||
                category == null || category.equals("") ||
                publishedBy == null || publishedBy.equals("")) {
            throw new NotNullConstraintException();
        }
        log.log(Level.INFO, "No null values.");
        if (!recipeName.matcher(name).matches()) {
            throw new UserNameException();
        }
        log.log(Level.INFO, "Recipe matches");
        this.usr = userDAO.getFromUsername(publishedBy);
        this.cat = categoryDAO.get(category);
        if (usr == null || usr.equals(new User())) {
            throw new FKUserException();
        }
        if (cat == null || cat.equals(new Category())) {
            throw new FKCategoryException();
        }
        log.log(Level.INFO,"Found user and category");
        if (picture == null || picture.equals("")) {
            picture = "images/reciepe/default.jpg";
        }
        try {
            entityManager.getTransaction().begin();
            Recipe rec = new Recipe();
            rec.setDescription(description);
            rec.setName(name);
            rec.setNumberOfProducts(numberOfProducts);
            rec.setPicture(picture);
            rec.setCategoryByCategory(cat);
            rec.setUserByPublishedBy(usr);
            rec.setPreparationTime(prep);
            rec.setHardness(hardness);
            rec.setCookingTime(cooking);
            log.log(Level.INFO, "Recipe created");
            entityManager.persist(rec);
            entityManager.flush();
            entityManager.getTransaction().commit();
            return this.findLast();
        } catch (Exception e) {
            log.log(Level.SEVERE, "Error creating Recipe" + e.getMessage());
            entityManager.getTransaction().rollback();
            throw e;
        }

    }

    public Long quantity() {
        return (Long) entityManager.createQuery("select count (idRecipe) from Recipe ").getSingleResult();
    }

    public List<Recipe> findAll() {
        try {
            return (List<Recipe>)entityManager.createQuery("select r from Recipe r ORDER BY idRecipe DESC").getResultList();
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return null;
        }

    }

    public Recipe findLast() {
        try {
            return (Recipe)entityManager.createQuery("select r from Recipe r ORDER BY idRecipe DESC").getResultList().get(0);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return null;
        }

    }

    public Long sharedBy(String username) {
        try {
            usr = userDAO.getFromUsername(username);
            return (Long) entityManager.createQuery("select count (idRecipe) from Recipe r where r.userByPublishedBy = :user")
                    .setParameter("user", usr).getSingleResult();
        } catch (Exception e) {
            log.log(Level.SEVERE, "Could not retrieve stat.\nError: " + e.getMessage());
            long res = 0;
            return res;
        }
    }

    public Recipe get(int id) {
        try {
            return (Recipe)entityManager.createQuery("select r from Recipe r where idRecipe=:id")
                    .setParameter("id",id).getSingleResult();
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return null;
        }
    }

    public List<Recipe> getNamed(String name) {
        try {
            return (List<Recipe>)entityManager.createQuery("select r from Recipe r where name=:r_name")
                    .setParameter("r_name",name).getResultList();
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return null;
        }
    }

    public List<Recipe> getDescribed(String description) {
        try {
            return (List<Recipe>)entityManager.createQuery("select r from Recipe r where name like :description")
                    .setParameter("description","%"+description+"%").getResultList();
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return null;
        }
    }

    public void Update(Recipe r, Recipe nr){
        entityManager.getTransaction().begin();
        String n, o;
        try {
            o=r.getName();
            n=nr.getName();
            if (!(n == null || (o.equals(n)))) {
                r.setName(n);
            }
            o=r.getDescription();
            n=nr.getDescription();
            if (!(n == null || (o.equals(n)))) {
                r.setDescription(n);
            }
            o=r.getPicture();
            n=nr.getPicture();
            if (!(n == null || (o.equals(n)))) {
                r.setPicture(n);
            }
            int io=r.getNumberOfProducts();
            int in=r.getNumberOfProducts();
            if (!(in == 0 || (io == in))) {
                r.setNumberOfProducts(in);
            }
            entityManager.merge(r);
            entityManager.flush();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            entityManager.getTransaction().rollback();
        }
    }

    public boolean delete(Recipe recipe) {
        try {
            boolean ok = isInDAO.deleteRecipeLink(recipe);
            if (!ok) {
                log.log(Level.SEVERE, "Could not delete linked ingredients.");
                return false;
            }
            ok = rankDAO.deleteRecipeRank(recipe);
            if (!ok) {
                log.log(Level.SEVERE, "Could not delete rank.");
                return false;
            }
            int res = entityManager.createQuery("DELETE FROM Recipe r where r.id = :recipe").setParameter("recipe", recipe.getIdRecipe()).executeUpdate();
            return res == 0;
        } catch (Exception e) {
            log.log(Level.WARNING, "Could not delete recipe.\nError: " + e.getMessage());
            return false;
        }
    }
}
