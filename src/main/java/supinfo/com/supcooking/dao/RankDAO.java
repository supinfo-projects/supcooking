package supinfo.com.supcooking.dao;

import supinfo.com.supcooking.Exeception.*;
import supinfo.com.supcooking.bean.Rank;
import supinfo.com.supcooking.bean.Recipe;
import supinfo.com.supcooking.bean.User;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

public class RankDAO {
    private Logger log = Logger.getLogger(this.getClass().getName());
    private static String persistenceUnitName = "SupCooking";
    private static EntityManager entityManager =  Persistence.createEntityManagerFactory(persistenceUnitName).createEntityManager();
    private static RecipeDAO recipeDAO = new RecipeDAO();
    private static UserDAO userDAO = new UserDAO();

    public void add(int recipeID, int recipeRank, String username) throws Exception {
        if (recipeID == 0 ||
                recipeRank == 0) {
            throw new NotNullConstraintException();

        }
        Recipe recipe = recipeDAO.get(recipeID);
        if (recipe == null || recipe.equals(new Recipe())) {
            throw new FKRecipeException();
        }
        User user = userDAO.getFromUsername(username);
        if (user == null || user.equals(new User())) {
            throw new FKUserException();
        }
        try {
            entityManager.getTransaction().begin();
            Rank rank = new Rank();
            rank.setRank(recipeRank);
            rank.setRecipeByRecipe(recipe);
            rank.setUserByRankedBy(user);
            entityManager.persist(rank);
            entityManager.flush();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            log.log(Level.SEVERE, e.getMessage());
            throw e;
        }

    }

    private int getRank(Recipe recipe, User user) {
        try {
            Rank r = (Rank)entityManager
                    .createQuery("select r from Rank r where recipeByRecipe=:recipe and userByRankedBy=:user")
                    .setParameter("recipe", recipe).setParameter("user", user).getSingleResult();
            return r.getRank();
        } catch (Exception e) {
            try {
                List<Rank> lr = this.get(recipe);
                if (lr == null) {
                    return 0;
                }
                int nbRank = 0;
                int rank = 0;
                for (Rank r: lr) {
                    nbRank++;
                    rank += r.getRank();
                }
                if (nbRank == 0) {
                    return 0;
                }
                return rank/nbRank;
            } catch (Exception ne) {
                return 0;
            }
        }
    }

    public int getRank(Recipe recipe, String username) {
        User usr;
        try {
             usr = userDAO.getFromUsername(username);
        } catch (Exception e) {
            usr = new User();
        }
        return this.getRank(recipe, usr);
    }

    public int getRank(int recipeId, String username) {
        Recipe r = recipeDAO.get(recipeId);
        return getRank(r, username);
    }

    public Long quantity() {
        return (Long) entityManager.createQuery("select count (idRank) from Rank ").getSingleResult();
    }

    public List<Rank> get(Recipe recipe) {
        try {
            return (List<Rank>)entityManager.createQuery("select r from Rank r where recipeByRecipe=:recipe")
                    .setParameter("recipe",recipe).getResultList();
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return null;
        }
    }

    public void Update(Rank r, Rank nr){
        entityManager.getTransaction().begin();
        try {
            int io=r.getRank();
            int in=nr.getRank();
            if (!(in == 0 || (io == in))) {
                r.setRank(in);
            }
            Recipe ro=r.getRecipeByRecipe();
            Recipe rn=nr.getRecipeByRecipe();
            if (!(rn == null || (ro.equals(rn)))) {
                r.setRecipeByRecipe(rn);
            }
            entityManager.merge(r);
            entityManager.flush();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            entityManager.getTransaction().rollback();
        }
    }


    private boolean delete(Rank rank) {
        try {
            int res = entityManager.createQuery("DELETE FROM Rank r where r.id = :rank").setParameter("rank", rank.getIdRank()).executeUpdate();
            return res == 0;
        } catch (Exception e) {
            log.log(Level.WARNING, "Could not delete recipe.\nError: " + e.getMessage());
            entityManager.getTransaction().rollback();
            return false;
        }
    }

    boolean deleteRecipeRank(Recipe recipe) {
        try {
            List<Rank> ranks = this.get(recipe);
            boolean ok;
            for (Rank rank: ranks) {
                ok = this.delete(rank);
                if (!ok) {
                    return false;
                }
            }
        } catch (Exception e) {
            log.log(Level.SEVERE,"Error while deleteing links.");
            return false;
        }
        return false;

    }
}
