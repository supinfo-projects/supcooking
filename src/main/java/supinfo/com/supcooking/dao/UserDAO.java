package supinfo.com.supcooking.dao;

import supinfo.com.supcooking.Exeception.*;
import supinfo.com.supcooking.bean.User;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.regex.Pattern;

public class UserDAO {
    private static String persistenceUnitName = "SupCooking";
    private static EntityManager entityManager =  Persistence.createEntityManagerFactory(persistenceUnitName).createEntityManager();
    private Logger log = Logger.getLogger(this.getClass().getName());
    private static String passwordRegex = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$";
    private static String emailRegex = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+[.][a-zA-Z]{2,6}$";
    private static String usernameRegex = "^[a-zA-Z]+[a-zA-Z1-9_@%]*[a-zA-Z1-9]+";
    private static Pattern passwordValidation = Pattern.compile(passwordRegex);
    private static Pattern emailValidation = Pattern.compile(emailRegex);
    private static Pattern usernameValidation = Pattern.compile(usernameRegex);

    /**
     * add user to database
     * @param username unique id for the user
     * @param email unique email address
     * @param password user password
     * @param firstname user first name
     * @param lastname user lastname
     * @param postalCode user zip code
     * @param level user cooking level
     * @param avatar user avatar link
     * @throws NotNullConstraintException
     * @throws UserNameException
     * @throws EmailConstraintViolation
     * @throws PasswordContraintViolation
     * @throws DuplicateEmailException
     * @throws DuplicateKeyException
     */
    public User add(String username, String email, String password, String firstname, String lastname,
                       int postalCode, int level, String avatar) throws Exception {

        if (username.equals("") ||
            email.equals("") ||
            password.equals("")) {
            throw new NotNullConstraintException();

        }
        if (!usernameValidation.matcher(username).matches()) {
            throw new UserNameException();
        }
        if (!emailValidation.matcher(email).matches()) {
            throw new EmailConstraintViolation();
        }
        if (!passwordValidation.matcher(password).matches()) {
            throw new PasswordContraintViolation();
        }
        if (this.getFromUsername(username) != null) {
            throw new DuplicateKeyException();
        }
        if (this.getFromEmail(email) != null) {
            throw new DuplicateEmailException();
        }
        try {
            entityManager.getTransaction().begin();
            User user = new User();
            user.setUsername(username);
            user.setLastname(lastname);
            user.setPassword(password);
            user.setEmail(email);
            user.setAvatar(avatar);
            user.setPostalCode(postalCode);
            user.setLevel(level);
            user.setFirstname(firstname);
            entityManager.persist(user);
            entityManager.flush();
            entityManager.getTransaction().commit();
            return user;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            log.log(Level.SEVERE, e.toString());
            throw  e;
        }
    }

    public Long quantity() {
        return  (Long) entityManager.createQuery("select count (username) from User").getSingleResult();
    }

    public User logIn(String id, String password) {
        try {
            User usr;
            usr = (User)entityManager.createQuery("select u from User u where username=:username")
                    .setParameter("username",id).getSingleResult();
            if (usr == null || usr == new User()) {
                usr = (User)entityManager.createQuery("select u from User u where email=:email")
                        .setParameter("email",id).getSingleResult();
            }
            if (usr != null && usr != new User() &&
                usr.getPassword().equals(password)) {
                return usr;
            }

            return null;
        } catch (Exception e) {
            log.log(Level.SEVERE, "Exception"+e.getMessage());
            return null;
        }
    }

    public User getFromUsername(String username) {
        try {
            return (User)entityManager.createQuery("select u from User u where username=:username")
                    .setParameter("username",username).getSingleResult();
        } catch (Exception e) {
            log.log(Level.SEVERE,e.getMessage());
            return null;
        }
    }

    private User getFromEmail(String email) {
        try {
            return (User)entityManager.createQuery("select u from User u where email=:email")
                    .setParameter("email",email).getSingleResult();
        } catch (Exception e) {
            log.log(Level.SEVERE,e.getMessage());
            return null;
        }
    }

    public void Update(User u, User nu){
        entityManager.getTransaction().begin();
        String n, o;
        try {
            o=u.getAvatar();
            n=nu.getAvatar();
            if (!(n == null || (o.equals(n)))) {
                u.setAvatar(n);
            }
            o=u.getEmail();
            n=nu.getEmail();
            if (!(n == null || (o.equals(n)))) {
                u.setEmail(n);
            }
            o=u.getLastname();
            n=nu.getLastname();
            if (!(n == null || (o.equals(n)))) {
                u.setLastname(n);
            }
            o=u.getFirstname();
            n=nu.getFirstname();
            if (!(n == null || (o.equals(n)))) {
                u.setFirstname(n);
            }
            o=u.getPassword();
            n=nu.getPassword();
            if (!(n == null || (o.equals(n)))) {
                u.setPassword(n);
            }
            int no=u.getPostalCode();
            int nn=nu.getPostalCode();
            if (!(nn == 0 || (no == nn))) {
                u.setPostalCode(nn);
            }
            no=u.getLevel();
            nn=nu.getLevel();
            if (!(nn == 0 || (no == nn))) {
                u.setLevel(nn);
            }
            entityManager.merge(u);
            entityManager.flush();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            entityManager.getTransaction().rollback();
        }
    }

}
