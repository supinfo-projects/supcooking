package supinfo.com.supcooking.dao;

import supinfo.com.supcooking.bean.Category;

import javax.persistence.*;
import java.util.*;
import java.util.logging.Logger;
import java.util.logging.Level;

public class CategoryDAO {
    private static String persistenceUnitName = "SupCooking";
    private static EntityManager entityManager = Persistence.createEntityManagerFactory(persistenceUnitName).createEntityManager();
    private Logger log = Logger.getLogger(this.getClass().getName());

    public CategoryDAO() {}


    public EntityManager getEntityManager() {
        return entityManager;
    }

    public Category add(String name, String description) {

        try {
            entityManager.getTransaction().begin();
            Category category = new Category();
            category.setName(name);
            category.setDescription(description);
            entityManager.persist(category);
            entityManager.flush();
            entityManager.getTransaction().commit();
            return category;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }

    }

    public Long quantity() {
        Long count = (Long) entityManager.createQuery("select count (name) from Category ").getSingleResult();
        return count;
    }

    public List<Category> findAll() {
        List<Category> listCats;
        try {
            listCats = entityManager.createQuery("select c from Category c").getResultList();
        } catch (Exception e){
            listCats = new ArrayList<Category>();
        }
        return listCats;

    }

    public List<String> findNames() {
        List<String> listCats;
        try {
            listCats = entityManager.createQuery("select name from Category c").getResultList();
        } catch (Exception e){
            listCats = new ArrayList<String>();
        }
        return listCats;

    }

    public Category get(String category) {
        System.out.println(category);
        try {
            Category cat = (Category) entityManager.createQuery("select c from Category c where name=:c_name")
                    .setParameter("c_name",category).getSingleResult();
            return cat;
        } catch (Exception e) {
            return null;
        }
    }

    public void Update(Category c, Category nc){
        entityManager.getTransaction().begin();
        String n, o;
        try {
            o=c.getName();
            n=nc.getName();
            if (!(n == null || (o.equals(n)))) {
                c.setName(n);
            }
            o=c.getDescription();
            n=nc.getDescription();
            if (!(n == null || (o.equals(n)))) {
                c.setDescription(n);
            }
            entityManager.merge(c);
            entityManager.flush();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        }
    }

}
