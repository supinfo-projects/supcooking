package supinfo.com.supcooking.dao;

import supinfo.com.supcooking.Exeception.*;
import supinfo.com.supcooking.bean.Ingredient;
import supinfo.com.supcooking.bean.Recipe;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.regex.Pattern;

public class IngredientDAO {
    private Logger log = Logger.getLogger(this.getClass().getName());
    private static String persistenceUnitName = "SupCooking";
    private static EntityManager entityManager =  Persistence.createEntityManagerFactory(persistenceUnitName).createEntityManager();

    public Ingredient add(String name, String description, int epiced, int allergen) throws Exception {
        log.log(Level.INFO, "Adding ingredient " + name);
        if (name == null || name.equals("")) {
            throw new NotNullConstraintException();
        }
        log.log(Level.INFO, "Passed name checking");
        if (description == null) {
            description = "";
        }
        try {
            entityManager.getTransaction().begin();
            log.log(Level.INFO, "Started transaction");
            Ingredient ing = new Ingredient();
            try {
                ing.setDescription(description);
            } catch (Exception e) {
                log.log(Level.SEVERE, "Could not set description");
                ing.setDescription("");
            }
            try {
                ing.setName(name);
            } catch (Exception e) {
                log.log(Level.SEVERE, "Could not set name");
                ing.setName("default_name");
            }
            try {
                ing.setAllergen((byte)allergen);
            } catch (Exception e) {
                log.log(Level.SEVERE, "Could not set allergen");
                ing.setAllergen((byte)0);
            }
            try {
                ing.setEpiced((byte)epiced);
            } catch (Exception e) {
                log.log(Level.SEVERE, "Could not set spicy");
                ing.setEpiced((byte)0);
            }
            entityManager.persist(ing);
            entityManager.flush();
            entityManager.getTransaction().commit();
            log.log(Level.INFO, "Transaction completed");
            return this.getLast();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            log.log(Level.SEVERE, e.getMessage());
            throw e;
        }

    }


    public Ingredient getLast() {
        try {
            return (Ingredient) entityManager.createQuery("select i from Ingredient i ORDER BY idIngredient DESC").getResultList().get(0);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return null;
        }

    }
    public Long quantity() {
        return (Long) entityManager.createQuery("select count (idIngredient) from Ingredient ").getSingleResult();
    }

    public List<String> findNames() {
        try {
            return (List<String>)entityManager.createQuery("select name from Ingredient i").getResultList();
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return null;
        }

    }

    public List<Ingredient> findAll() {
        try {
            return (List<Ingredient>)entityManager.createQuery("select i from Ingredient i").getResultList();
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return null;
        }

    }
    public Ingredient getFirst(String name) {
        try {
            return (Ingredient)entityManager.createQuery("select i from Ingredient i where name=:i_name")
                    .setParameter("i_name",name).getResultList().get(0);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return null;
        }
    }


    public List<Ingredient> get(String name) {
        try {
            return (List<Ingredient>)entityManager.createQuery("select i from Ingredient i where name=:i_name")
                    .setParameter("i_name",name).getResultList();
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return null;
        }
    }

    public void Update(Ingredient r, Ingredient nr){
        entityManager.getTransaction().begin();
        String n, o;
        try {
            o=r.getName();
            n=nr.getName();
            if (!(n == null || (o.equals(n)))) {
                r.setName(n);
            }
            o=r.getDescription();
            n=nr.getDescription();
            if (!(n == null || (o.equals(n)))) {
                r.setDescription(n);
            }
            byte io=r.getAllergen();
            byte in=r.getAllergen();
            if (!(in == 0 || (io == in))) {
                r.setAllergen(in);
            }
            io=r.getEpiced();
            in=r.getEpiced();
            if (!(in == 0 || (io == in))) {
                r.setEpiced(in);
            }
            entityManager.merge(r);
            entityManager.flush();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            entityManager.getTransaction().rollback();
        }
    }

}
