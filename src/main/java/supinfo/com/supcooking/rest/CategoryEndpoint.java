package supinfo.com.supcooking.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.json.JSONException;
import org.json.JSONObject;
import supinfo.com.supcooking.bean.Category;
import supinfo.com.supcooking.dao.CategoryDAO;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@Path("category")
public class CategoryEndpoint {
    private static CategoryDAO categoryDAO = new CategoryDAO();
    private static GsonBuilder gsonBuilder = new GsonBuilder();
    private Logger log = Logger.getLogger(this.getClass().getName());

    @Path("")
    @GET
    @Produces("application/json")
    public List<Category> getAllCategory() {
        return categoryDAO.findAll();
    }
}