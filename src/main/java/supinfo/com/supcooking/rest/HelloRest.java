package supinfo.com.supcooking.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import java.util.Date;

@Path("")
public class HelloRest {
    @Path("ping")
    @GET
    public Response ping() {
        String out = "REST service is running.<Ping @"+
                new Date().toString() + "";
        return Response.accepted().entity(out).build();
    }

    @Path("hello")
    @GET
    public Response hello(Request request) {
       String out = "<h1>Hello Rest!</h1><br/><br/><h2>Welcome on Sup Cooking REST API</h2>" +
                    "<br/><br/>This service is build with Tomcat 8, JEE 8 and maven.";
       return Response.accepted().entity(out).build();
    }
}
