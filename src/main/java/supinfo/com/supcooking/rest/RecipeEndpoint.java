package supinfo.com.supcooking.rest;

import supinfo.com.supcooking.bean.Category;
import supinfo.com.supcooking.bean.Ingredient;
import supinfo.com.supcooking.bean.Recipe;
import supinfo.com.supcooking.bean.User;
import supinfo.com.supcooking.dao.IsInDAO;
import supinfo.com.supcooking.dao.RecipeDAO;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@Path("recipe")
public class RecipeEndpoint {
    private static RecipeDAO recipeDAO = new RecipeDAO();
    private Logger log = Logger.getLogger(this.getClass().getName());


    private List<RecipeReturn> recipeListToReturn(List<Recipe> recipes) {
        ArrayList<RecipeReturn> res = new ArrayList<>();
        for (Recipe rec: recipes) {
            res.add(new RecipeReturn(rec));
        }
        return res;
    }

    @Path("")
    @GET
    @Produces("application/json")
    public List<RecipeReturn> getAllRecipe() {
        List<Recipe> recipes =  recipeDAO.findAll();
        return recipeListToReturn(recipes);
    }

    @Path("/name/{name}")
    @GET
    @Produces("application/json")
    public List<RecipeReturn> getRecipe(@PathParam("name") String name) {
        List<Recipe> recipes = recipeDAO.getNamed(name);
        return recipeListToReturn(recipes);
    }

    @Path("/description")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces("application/json")
    public List<RecipeReturn> getRecipeFromDescription(Description description) {
        log.log(Level.INFO, description.toString());
        List<Recipe> recipes = recipeDAO.getDescribed(description.getDescription());
        return recipeListToReturn(recipes);
    }
}

class UserReturn {
    private String username;
    private String email;
    private String firstname;
    private String lastname;
    private int postalCode;
    private int level;
    private String avatar;

    UserReturn() {}

    UserReturn(User usr) {
        this.username = usr.getUsername();
        this.email = usr.getEmail();
        try {
            this.firstname = usr.getFirstname();
        } catch (Exception e) { this.firstname = "Jack"; }
        try {
            this.lastname = usr.getLastname();
        } catch (Exception e) { this.lastname = "Cestparoù"; }
        try {
            this.postalCode = usr.getPostalCode();
        } catch (Exception e) { this.postalCode = 14800; }
        try {
            this.level = usr.getLevel();
        } catch (Exception e) { this.level = 1; }
        try {
            this.avatar = usr.getAvatar();
        } catch (Exception e) { this.avatar = "images/avatars/user.png"; }
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public int getPostalCode() {
        return postalCode;
    }

    public int getLevel() {
        return level;
    }

    public String getAvatar() {
        return avatar;
    }

}
class CategoryReturn {
    private String name;
    private String description;

    CategoryReturn() {}

    CategoryReturn(Category cat) {
        this.name = cat.getName();
        try {
            this.description = cat.getDescription();
        } catch (Exception e) { this.description=""; }
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
class Description {
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
class RecipeReturn {
    private static IsInDAO isInDAO = new IsInDAO();
    private int id;
    private String name;
    private String description;
    private String picture;
    private int hardness;
    private int prepTime;
    private int cookingTime;
    private UserReturn publishedBy;
    private CategoryReturn category;
    private List<Ingredient> ingredients;

    RecipeReturn() {}

    RecipeReturn(Recipe recipe) {
        this.id = recipe.getIdRecipe();
        this.name = recipe.getName();
        try {
            this.description = recipe.getDescription();
        } catch (Exception e) {
            this.description = "";
        }
        try {
            this.picture = recipe.getPicture();
        } catch (Exception e) {
            this.picture = "images/recipe/default.jpg";
        }
        try {
            this.hardness = recipe.getHardness();
        } catch (Exception e) {
            this.hardness = 1;
        }
        try {
            this.prepTime = recipe.getPreparationTime();
        } catch (Exception e) {
            this.prepTime = 30;
        }
        try {
            this.cookingTime = recipe.getCookingTime();
        } catch (Exception e) {
            this.cookingTime = 30;
        }
        try {
            this.publishedBy = new UserReturn(recipe.getUserByPublishedBy());
        } catch (Exception e) {
            this.publishedBy = new UserReturn();
        }
        try {
            this.category = new CategoryReturn(recipe.getCategoryByCategory());
        } catch (Exception e) {
            this.category = new CategoryReturn();
        }
        try {
            this.ingredients = isInDAO.getIngredients(recipe);
        } catch (Exception e) {
            this.ingredients = new ArrayList<>();
        }
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getPicture() {
        return picture;
    }

    public int getHardness() {
        return hardness;
    }

    public int getPrepTime() {
        return prepTime;
    }

    public int getCookingTime() {
        return cookingTime;
    }

    public UserReturn getPublishedBy() {
        return publishedBy;
    }

    public CategoryReturn getCategory() {
        return category;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

}
