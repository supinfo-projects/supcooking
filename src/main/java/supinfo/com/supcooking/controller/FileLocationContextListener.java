package supinfo.com.supcooking.controller;

import java.io.File;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class FileLocationContextListener implements ServletContextListener {

    public void contextInitialized(ServletContextEvent servletContextEvent) {
        String rootPath = System.getProperty("catalina.home") + "/supcooking";
        ServletContext ctx = servletContextEvent.getServletContext();
        File file = new File(rootPath + File.separator + "/images");
        if(!file.exists()) file.mkdirs();
        ctx.setAttribute("FILES_DIR", rootPath + File.separator + "/images");
    }

    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        //do cleanup if needed
    }

}