package supinfo.com.supcooking.controller;


import java.io.IOException;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import supinfo.com.supcooking.bean.User;
import supinfo.com.supcooking.dao.UserDAO;

/**
 * Servlet implementation class UserControllerServlet
 */
@WebServlet("/Login")
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static UserDAO userDAO = new UserDAO();
    private HttpSession session;
    private Logger log = Logger.getLogger(this.getClass().getName());

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        doPost(request, response);

    }
    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String sto = request.getParameter("id");
        if (sto == null) {
            sto = (String) request.getAttribute("id");
        }

        String pass = request.getParameter("password");
        if (pass == null) {
            pass = (String) request.getAttribute("password");
        }
        Boolean newUser = (Boolean) request.getAttribute("new_user");
        User ref = userDAO.logIn(sto,pass);
        session=request.getSession();
        if (ref != null) {
            session.setAttribute("new_user", newUser);
            session.setAttribute("username", ref.getUsername());
            session.setAttribute("level", ref.getLevel());
            session.setAttribute("avatar", ref.getAvatar());
            session.setAttribute("firstname", ref.getFirstname());
            session.setAttribute("lastname", ref.getLastname());
            boolean connection = false;
            try {
                connection = (Boolean) request.getAttribute("connected");
            } catch (Exception e) { }
            if (connection) {
                response.sendRedirect("index.jsp");
                return;
            }
            response.sendRedirect("connected/");

        } else {
            session.setAttribute("user_id", null);
            response.sendRedirect("index.jsp");
        }
    }

}