package supinfo.com.supcooking.controller;
import org.hibernate.exception.ConstraintViolationException;
import supinfo.com.supcooking.Exeception.*;
import supinfo.com.supcooking.bean.User;
import supinfo.com.supcooking.dao.UserDAO;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.logging.Level;
import java.util.logging.Logger;



/**
 * Servlet implementation class UserControllerServlet
 */
@WebServlet("/Register")
public class RegisterServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static UserDAO userDAO = new UserDAO();
    private HttpSession session;
    private Logger log = Logger.getLogger(this.getClass().getName());


    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String name = request.getParameter("name");
        String lastname = request.getParameter("lastname");
        String password = request.getParameter("password");
        String email = request.getParameter("email");
        int zipcode;
        try {
            zipcode = Integer.parseInt(request.getParameter("zip"));
        } catch (Exception e) {
            zipcode = 0;
        }
        int level;
        try {
            level = Integer.parseInt(request.getParameter("level"));
        } catch (Exception e) {
            level = 0;
        }
        setSession(request.getSession(true));
        try {
            userDAO.add(username, email, password, name, lastname, zipcode, level, "rng.png");
            request.setAttribute("new_user",true);
            request.setAttribute("id",username);
            request.setAttribute("password",password);
            getServletConfig().getServletContext().getRequestDispatcher("/Login").forward(request,response);

        } catch (NotNullConstraintException e) {
            log.log(Level.SEVERE, "Null entry for required entry");
            request.setAttribute("username_is_null", false );
            request.setAttribute("username", username );
            if (username == null || username.equals("")) {
                request.setAttribute("username_is_null", true );
            }
            request.setAttribute("email_is_null", false);
            request.setAttribute("email", email );
            if (email == null || email.equals("")) {
                request.setAttribute("username_is_null", true );
            }
            request.setAttribute("pwd_is_null", false );
            request.setAttribute("password", password);
            if (password == null || password.equals("")) {
                request.setAttribute("pwd_is_null", true );
            }
            getServletConfig().getServletContext().getRequestDispatcher("/register.jsp").forward(request,response);
        } catch (DuplicateKeyException e) {
            log.log(Level.SEVERE, "Duplicate user");
            request.setAttribute("duplicate_user_name", true);
            getServletConfig().getServletContext().getRequestDispatcher("/register.jsp").forward(request,response);
        } catch (DuplicateEmailException e) {
            log.log(Level.SEVERE, "Duplicate email");
            request.setAttribute("duplicate_email", true);
            getServletConfig().getServletContext().getRequestDispatcher("/register.jsp").forward(request,response);
        } catch (PasswordContraintViolation e) {
            log.log(Level.SEVERE, "Password unmached. >>" + password);
            request.setAttribute("pwd_invalid", true);
            request.setAttribute("pwd_request", "Password must contain a lower and uper case letter, a digit, and a special carater in @#$%^&+=]");
            getServletConfig().getServletContext().getRequestDispatcher("/register.jsp").forward(request,response);
        } catch (EmailConstraintViolation e) {
            log.log(Level.SEVERE, "Email unmached. >>" + email);
            request.setAttribute("email_invalid", true);
            request.setAttribute("email_request", "Email is invalid. Please ensure your mail is like: something@domain.loc");
            getServletConfig().getServletContext().getRequestDispatcher("/register.jsp").forward(request,response);
        }  catch (UserNameException e) {
            log.log(Level.SEVERE, "Username unmached. >>" + username);
            request.setAttribute("username_invalid", true);
            request.setAttribute("username_request", "Username must start with a digit and end with digits or letters. It can only contain _%@ as special characters.");
            getServletConfig().getServletContext().getRequestDispatcher("/register.jsp").forward(request,response);
        }  catch (ConstraintViolationException e) {
            e.printStackTrace();
            request.setAttribute("other_constraint_violation", true);
            request.setAttribute("caused_by",e.getCause().toString());
            request.setAttribute("value",e.getMessage());
            getServletConfig().getServletContext().getRequestDispatcher("/register.jsp").forward(request,response);
        } catch (Exception e){
            request.setAttribute("fail", e.getCause().toString());
            request.setAttribute("message", e.getMessage());
            getServletConfig().getServletContext().getRequestDispatcher("/500.jsp").forward(request,response);
        }
    }

    public HttpSession getSession() {
        return session;
    }

    public void setSession(HttpSession session) {
        this.session = session;
    }

}
