package supinfo.com.supcooking.controller;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import supinfo.com.supcooking.Exeception.FKCategoryException;
import supinfo.com.supcooking.Exeception.FKUserException;
import supinfo.com.supcooking.Exeception.NotNullConstraintException;
import supinfo.com.supcooking.Exeception.UserNameException;
import supinfo.com.supcooking.bean.Category;
import supinfo.com.supcooking.bean.Recipe;
import supinfo.com.supcooking.dao.CategoryDAO;
import supinfo.com.supcooking.dao.RecipeDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebServlet("/connected/AddIngredientRecipe")
@MultipartConfig
public class AddIngredientRecipeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static RecipeDAO recipeDAO = new RecipeDAO();
    private static CategoryDAO categoryDAO = new CategoryDAO();
    private HttpSession session;
    private Logger log = Logger.getLogger(this.getClass().getName());
    private ServletFileUpload uploader = null;



    public void init() throws ServletException{
        DiskFileItemFactory fileFactory = new DiskFileItemFactory();
        File filesDir = (File) getServletContext().getAttribute("FILES_DIR_FILE");
        fileFactory.setRepository(filesDir);
        this.uploader = new ServletFileUpload(fileFactory);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        setSession(request.getSession());
        String category = request.getParameter("category");
        String newCategoryName = request.getParameter("new_category");
        Category cat = new Category();
        if (category.equals("new_category") && !newCategoryName.equals("")) {
            category = newCategoryName;
            try {
                cat = categoryDAO.add(newCategoryName, "");
            } catch (Exception e) {
                cat = categoryDAO.get(newCategoryName);
            }
        }
        String username = (String)session.getAttribute("username");
        String name = request.getParameter("name");
        // Part avatar = request.getPart("avatar");
        String description = request.getParameter("description");
        String outFilePath;
        FileItem avatar = null;
        boolean canUploadImage = false;
        try {
            avatar = uploader.parseRequest(request).get(0);
            outFilePath = "images/recipe/"+name+ FilenameUtils.getExtension(avatar.getName());
            canUploadImage = true;
        } catch (Exception e) {
            log.log(Level.SEVERE, "Could not add image");
            log.log(Level.SEVERE, e.getMessage());
            outFilePath = "images/recipe/default.png";
        }
        /* TODO find a write available place
           This code work fine on a fully managed web server but does not comply on cargo :/
         */
       /* String outFilePath = "images/recipe/" + name + avatar.getContentType();
        File file = new File("./"+outFilePath);

        try (InputStream input = fileContent) {
            Files.copy(input, file.toPath());
        } catch (Exception e) {
        }*/
        int nbIngredient;
        try {
            nbIngredient = Integer.parseInt(request.getParameter("n_ingredient"));
        } catch (Exception e) {
            nbIngredient = 0;
        }
        int hardness;
        try {
            hardness = Integer.parseInt(request.getParameter("hardness"));
        } catch (Exception e) {
            hardness = 0;
        }
        int prep;
        try {
            prep = Integer.parseInt(request.getParameter("prep"));
        } catch (Exception e) {
            prep = 0;
        }

        int cooking;
        try {
            cooking = Integer.parseInt(request.getParameter("cooking"));
        } catch (Exception e) {
            cooking = 0;
        }
        try {
            Recipe rec = recipeDAO.add(name, category, description, nbIngredient, username, outFilePath, hardness, prep, cooking);
            if (canUploadImage) {
                File outAvatar = new File(request.getServletContext().getAttribute("FILES_DIR")+File.separator+outFilePath);
                avatar.write(outAvatar);
            }
            request.setAttribute("nb_ingredient", nbIngredient);
            session.setAttribute("recipe", rec);
            session.setAttribute("nb_ingredient", nbIngredient);
            getServletConfig().getServletContext().getRequestDispatcher("/connected/add_ingredient.jsp").include(request,response);
        } catch (NotNullConstraintException e) {
            log.log(Level.SEVERE, "A constraint was not full filled");
            log.log(Level.INFO, "Username: " + username);
            log.log(Level.INFO, "Recipe name: " + name);
            log.log(Level.INFO, "Category name: " +category + " --- " + cat.getName());

            getServletConfig().getServletContext().getRequestDispatcher("/connected/add_recipe.jsp").forward(request,response);
        } catch (UserNameException e) {
            log.log(Level.SEVERE, "Recipe name does not match regex");
            getServletConfig().getServletContext().getRequestDispatcher("/connected/add_recipe.jsp").forward(request,response);
        } catch (FKUserException e) {
            log.log(Level.SEVERE, "User registration met an error");
            getServletConfig().getServletContext().getRequestDispatcher("/connected/add_recipe.jsp").forward(request,response);
        } catch (FKCategoryException e) {
            log.log(Level.SEVERE, "Category registration met an error");
            getServletConfig().getServletContext().getRequestDispatcher("/connected/add_recipe.jsp").forward(request,response);
        } catch (NullPointerException e) {
            log.log(Level.SEVERE, "WtF is ThIs NuLl PoInTeR hErE ?");
            log.log(Level.SEVERE, e.getStackTrace().toString());
            throw e;
        } catch (Exception e) {
            log.log(Level.SEVERE, "Full error: " + e.toString());
            log.log(Level.SEVERE, "Error message from AddIngredientRecipeServlet "+e.getMessage());
            getServletConfig().getServletContext().getRequestDispatcher("/500.jsp").forward(request,response);
        }
    }

    public HttpSession getSession() {
        return session;
    }

    public void setSession(HttpSession session) {
        this.session = session;
    }
}
