package supinfo.com.supcooking.controller;

import org.hibernate.exception.ConstraintViolationException;
import supinfo.com.supcooking.Exeception.*;
import supinfo.com.supcooking.bean.Ingredient;
import supinfo.com.supcooking.bean.IsIn;
import supinfo.com.supcooking.bean.Recipe;
import supinfo.com.supcooking.dao.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebServlet("/connected/AddRecipe")
public class AddRecipeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static IsInDAO isInDAO = new IsInDAO();
    private static IngredientDAO ingredientDAO = new IngredientDAO();
    private HttpSession session;
    private Logger log = Logger.getLogger(this.getClass().getName());


    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        setSession(request.getSession(false));
        log.log(Level.INFO,"Starting recipe add");
        try {
            int nbIngredient;
            try {
                nbIngredient = (Integer)session.getAttribute("nb_ingredient");
                log.log(Level.INFO, "Found " + Integer.toString(nbIngredient));
            } catch (Exception e) {
                log.log(Level.SEVERE, e.getMessage());
                log.log(Level.WARNING, "No ingredients found, defaulting on 0");
                nbIngredient = 0;
            }
            Recipe rec = new Recipe();
            try {
                rec = (Recipe)session.getAttribute("recipe");
                log.log(Level.INFO, "Found recipe: " + rec.getIdRecipe());
            } catch (Exception e) {
                log.log(Level.SEVERE, "FATAL:: No recipe found");
                request.setAttribute("fail", "Did not found recipe from session");
                if (e.getCause() != null) {
                    request.setAttribute("fail", e.getCause().toString());
                }
                request.setAttribute("message", e.getMessage());
                getServletConfig().getServletContext().getRequestDispatcher("/500.jsp").forward(request,response);

            }
            String ingredientRef;
            Ingredient ing;
            String ingredientName;
            String ingredientDesc;
            boolean isEpiced;
            boolean isAllergen;
            String unit;
            int quantity;
            IsIn isIn;
            int epiced;
            int allergen;
            for (int i = 0; i < nbIngredient; i++) {
                log.log(Level.INFO, "Into for ingredients " + i + " loop");
                ingredientRef = request.getParameter("ingredient_"+Integer.toString(i));
                if (ingredientRef.equals("new_ingredient")) {
                    log.log(Level.INFO, "Creating ingredient");
                    ingredientName = request.getParameter("new_ingredient_name_"+Integer.toString(i));
                    try {
                        ingredientDesc = request.getParameter("new_ingredient_desc_"+Integer.toString(i));
                    } catch (Exception e) {
                        log.log(Level.INFO, "No description defined");
                        ingredientDesc= "";
                    }
                    try {
                        isEpiced = request.getParameter("new_ingredient_"+Integer.toString(i)+"_is_epiced").equals("true");
                    } catch (Exception e) {
                        log.log(Level.WARNING, "Spicy set to false");
                        isEpiced = false;
                    }
                    try {
                        isAllergen = request.getParameter("new_ingredient_"+Integer.toString(i)+"_is_allergen").equals("true");
                    } catch (Exception e) {
                        log.log(Level.WARNING, "Allergen set to false");
                        isAllergen = false;
                    }
                    log.log(Level.INFO, "Got all values");
                    ing = ingredientDAO.add(ingredientName,ingredientDesc,isEpiced ? 1 : 0, isAllergen ? 1 : 0);
                    log.log(Level.SEVERE, "Ingredient is defined ? " + Integer.toString(ing.getIdIngredient()));
                } else {
                    log.log(Level.INFO, "Ingredient already defined");
                    ing = ingredientDAO.getFirst(ingredientRef);
                }

                try {
                    unit = request.getParameter("unit_"+Integer.toString(i));
                } catch (Exception e) {
                    unit = "u";
                }
                try {
                    quantity = Integer.parseInt(request.getParameter("quantity"+Integer.toString(i)));
                } catch (Exception e) {
                    quantity = 1;
                }
                isInDAO.add(rec.getIdRecipe(), ing.getName(), quantity, unit);
                log.log(Level.INFO, "Added ingredient to recipe");
            }
            session.removeAttribute("recipe");
            session.removeAttribute("nb_ingredient");
            getServletConfig().getServletContext().getRequestDispatcher("/connected/index.jsp").forward(request, response);
        } catch (Exception e) {
            request.setAttribute("fail", e.getCause().toString());
            request.setAttribute("message", e.getMessage());
            getServletConfig().getServletContext().getRequestDispatcher("/500.jsp").forward(request,response);
        }
    }

    public HttpSession getSession() {
        return session;
    }

    public void setSession(HttpSession session) {
        this.session = session;
    }
}
