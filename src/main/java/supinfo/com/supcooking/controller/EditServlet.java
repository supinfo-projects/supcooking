package supinfo.com.supcooking.controller;

import org.hibernate.exception.ConstraintViolationException;
import supinfo.com.supcooking.Exeception.*;
import supinfo.com.supcooking.bean.User;
import supinfo.com.supcooking.dao.UserDAO;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.logging.Level;
import java.util.logging.Logger;



/**
 * Servlet implementation class UserControllerServlet
 */
@WebServlet("/connected/Edit")
public class EditServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static UserDAO userDAO = new UserDAO();
    private HttpSession session;
    private Logger log = Logger.getLogger(this.getClass().getName());

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        setSession(request.getSession(false));
        String username;
        try {
            username = (String) session.getAttribute("username");
        } catch (Exception e) {
            request.setAttribute("fail", e.getCause().toString());
            request.setAttribute("message", e.getMessage());
            getServletConfig().getServletContext().getRequestDispatcher("/500.jsp").forward(request,response);
            return;
        }
        String name = request.getParameter("name");
        String lastname = request.getParameter("lastname");
        String password = request.getParameter("password");
        String email = request.getParameter("email");
        int zipcode;
        try {
            zipcode = Integer.parseInt(request.getParameter("zip"));
        } catch (Exception e) {
            zipcode = 0;
        }
        int level;
        try {
            log.log(Level.INFO, request.getParameter("level"));
            level = Integer.parseInt(request.getParameter("level"));
        } catch (Exception e) {
            level = 0;
        }
        User usr = new User();
        usr.setUsername(username);
        usr.setFirstname(name);
        usr.setLastname(lastname);
        usr.setLevel(level);
        usr.setPassword(password);
        usr.setEmail(email);
        usr.setPostalCode(zipcode);
        try {
            User u = userDAO.getFromUsername(username);
            userDAO.Update(u, usr);
            request.setAttribute("new_user",false);
            request.setAttribute("id",username);
            request.setAttribute("password",password);
            request.setAttribute("connected", true);
            getServletConfig().getServletContext().getRequestDispatcher("/Login").forward(request,response);
        } catch (ConstraintViolationException e) {
            e.printStackTrace();
            request.setAttribute("other_constraint_violation", true);
            request.setAttribute("caused_by",e.getCause().toString());
            request.setAttribute("value",e.getMessage());
            getServletConfig().getServletContext().getRequestDispatcher("/connected/edit.jsp").forward(request,response);
        } catch (Exception e){
            request.setAttribute("fail", e.getCause().toString());
            request.setAttribute("message", e.getMessage());
            getServletConfig().getServletContext().getRequestDispatcher("/500.jsp").forward(request,response);
        }
    }

    public HttpSession getSession() {
        return session;
    }

    public void setSession(HttpSession session) {
        this.session = session;
    }

}

