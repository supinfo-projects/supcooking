package supinfo.com.supcooking.controller;

import org.hibernate.exception.ConstraintViolationException;
import supinfo.com.supcooking.Exeception.*;
import supinfo.com.supcooking.bean.Ingredient;
import supinfo.com.supcooking.bean.IsIn;
import supinfo.com.supcooking.bean.Recipe;
import supinfo.com.supcooking.dao.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebServlet("/connected/Rank")
public class RankServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static RankDAO rankDAO = new RankDAO();
    private HttpSession session;
    private Logger log = Logger.getLogger(this.getClass().getName());


    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        setSession(request.getSession(false));
        try {
            Recipe recipe;
            recipe = (Recipe)(session.getAttribute("recipe"));
            if (recipe == null || recipe.equals(new Recipe())) {
                throw new FKRecipeException();
            }
            log.log(Level.INFO, "Found " + Integer.toString(recipe.getIdRecipe()));

            String userName = (String)session.getAttribute("username");
            int rank;
            try {
                rank = Integer.parseInt(request.getParameter("recipe_rating"));
            } catch (Exception e) {
                rank = 0;
            }
            rankDAO.add(recipe.getIdRecipe(), rank, userName);
            getServletConfig().getServletContext().getRequestDispatcher("/connected/index.jsp").forward(request, response);
        } catch (Exception e) {
            request.setAttribute("fail", e.getCause().toString());
            request.setAttribute("message", e.getMessage());
            getServletConfig().getServletContext().getRequestDispatcher("/500.jsp").forward(request,response);
        }
    }

    public HttpSession getSession() {
        return session;
    }

    public void setSession(HttpSession session) {
        this.session = session;
    }
}
