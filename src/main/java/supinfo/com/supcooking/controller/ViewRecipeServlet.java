package supinfo.com.supcooking.controller;


import java.io.IOException;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class UserControllerServlet
 */
@WebServlet("/connected/ViewRecipe")
public class ViewRecipeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private Logger log = Logger.getLogger(this.getClass().getName());

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        doPost(request, response);
    }
    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        log.log(Level.SEVERE, "Tring to view recipe");
        try {
            int recipeId;
            try {
                 recipeId = Integer.parseInt(request.getParameter("id_recipe"));
            } catch (Exception e) {
                log.log(Level.SEVERE, "No recipe to show");
                response.sendRedirect("500.jsp");
                return;
            }
            log.log(Level.INFO, "Got recipe ID");
            request.setAttribute("id_recipe", recipeId);
            getServletConfig().getServletContext().getRequestDispatcher("/connected/view_recipe.jsp").forward(request, response);
        } catch (Exception e) {
            log.log(Level.SEVERE, "Error: "+e.getMessage());
            response.sendRedirect("index.jsp");
        }
    }

}