package supinfo.com.supcooking.controller;

import org.hibernate.exception.ConstraintViolationException;
import supinfo.com.supcooking.Exeception.*;
import supinfo.com.supcooking.bean.User;
import supinfo.com.supcooking.dao.UserDAO;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.logging.Level;
import java.util.logging.Logger;



/**
 * Servlet implementation class UserControllerServlet
 */
@WebServlet("/connected/SetEdit")
public class SetEditServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static UserDAO userDAO = new UserDAO();
    private HttpSession session;
    private Logger log = Logger.getLogger(this.getClass().getName());

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        session = request.getSession();
        User user = userDAO.getFromUsername((String)session.getAttribute("username"));
        if (user == null) {
            getServletConfig().getServletContext().getRequestDispatcher("/connected/edit.jsp").forward(request,response);
        } else {
            request.setAttribute("username", user.getUsername());
            request.setAttribute("firstName", user.getFirstname());
            request.setAttribute("lastName", user.getLastname());
            request.setAttribute("email", user.getEmail());
            request.setAttribute("password", user.getPassword());
            request.setAttribute("avatar", user.getAvatar());
            request.setAttribute("level", user.getLevel());
            request.setAttribute("zipCode", user.getPostalCode());
            getServletConfig().getServletContext().getRequestDispatcher("/connected/edit.jsp").forward(request,response);
        }
    }

    public HttpSession getSession() {
        return session;
    }

    public void setSession(HttpSession session) {
        this.session = session;
    }

}
