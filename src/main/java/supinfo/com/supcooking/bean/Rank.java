package supinfo.com.supcooking.bean;

import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Rank {
    private int idRank;
    private Integer rank;
    private Recipe recipeByRecipe;
    private User userByRankedBy;

    @Id
    @Column(name = "id_rank", nullable = false)
    public int getIdRank() {
        return idRank;
    }

    public void setIdRank(int idRank) {
        this.idRank = idRank;
    }

    @Basic
    @Column(name = "rank", nullable = true)
    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rank rank1 = (Rank) o;
        return idRank == rank1.idRank &&
                Objects.equals(rank, rank1.rank);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idRank, rank);
    }

    @ManyToOne
    @JoinColumn(name = "recipe", referencedColumnName = "id_recipe", nullable = false)
    public Recipe getRecipeByRecipe() {
        return recipeByRecipe;
    }

    public void setRecipeByRecipe(Recipe recipeByRecipe) {
        this.recipeByRecipe = recipeByRecipe;
    }

    @ManyToOne
    @JoinColumn(name = "ranked_by", referencedColumnName = "username")
    public User getUserByRankedBy() {
        return userByRankedBy;
    }

    public void setUserByRankedBy(User userByRankedBy) {
        this.userByRankedBy = userByRankedBy;
    }
}
