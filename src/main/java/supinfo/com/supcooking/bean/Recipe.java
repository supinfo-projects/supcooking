package supinfo.com.supcooking.bean;

import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
public class Recipe {
    private int idRecipe;
    private String name;
    private String description;
    private int numberOfProducts;
    private String picture;
    private Category categoryByCategory;
    private User userByPublishedBy;
    private Timestamp createdAt;
    private Timestamp updatedAt;
    private Integer hardness;
    private Integer preparationTime;
    private Integer cookingTime;

    @Id
    @Column(name = "id_recipe", nullable = false)
    public int getIdRecipe() {
        return idRecipe;
    }

    public void setIdRecipe(int idRecipe) {
        this.idRecipe = idRecipe;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description", nullable = false, length = -1)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "number_of_products", nullable = false)
    public int getNumberOfProducts() {
        return numberOfProducts;
    }

    public void setNumberOfProducts(int numberOfProducts) {
        this.numberOfProducts = numberOfProducts;
    }

    @Basic
    @Column(name = "picture", nullable = true, length = 255)
    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Recipe recipe = (Recipe) o;
        return idRecipe == recipe.idRecipe &&
                numberOfProducts == recipe.numberOfProducts &&
                Objects.equals(name, recipe.name) &&
                Objects.equals(description, recipe.description) &&
                Objects.equals(picture, recipe.picture);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idRecipe, name, description, numberOfProducts, picture);
    }

    @ManyToOne
    @JoinColumn(name = "category", referencedColumnName = "name", nullable = false)
    public Category getCategoryByCategory() {
        return categoryByCategory;
    }

    public void setCategoryByCategory(Category categoryByCategory) {
        this.categoryByCategory = categoryByCategory;
    }

    @ManyToOne
    @JoinColumn(name = "published_by", referencedColumnName = "username", nullable = false)
    public User getUserByPublishedBy() {
        return userByPublishedBy;
    }

    public void setUserByPublishedBy(User userByPublishedBy) {
        this.userByPublishedBy = userByPublishedBy;
    }

    @Basic
    @Column(name = "created_at", nullable = false)
    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    @Basic
    @Column(name = "updated_at", nullable = false)
    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Basic
    @Column(name = "hardness", nullable = true)
    public Integer getHardness() {
        return hardness;
    }

    public void setHardness(Integer hardness) {
        this.hardness = hardness;
    }

    @Basic
    @Column(name = "preparation_time", nullable = true)
    public Integer getPreparationTime() {
        return preparationTime;
    }

    public void setPreparationTime(Integer preparationTime) {
        this.preparationTime = preparationTime;
    }

    @Basic
    @Column(name = "cooking_time", nullable = true)
    public Integer getCookingTime() {
        return cookingTime;
    }

    public void setCookingTime(Integer cookingTime) {
        this.cookingTime = cookingTime;
    }
}
