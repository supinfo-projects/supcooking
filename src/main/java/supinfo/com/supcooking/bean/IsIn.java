package supinfo.com.supcooking.bean;

import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "is_in", schema = "sup_cooking", catalog = "")
public class IsIn {
    private int idIsIn;
    private int quantity;
    private String unit;
    private Recipe recipeByRecipe;
    private Ingredient ingredientByIngredient;

    @Id
    @Column(name = "id_is_in", nullable = false)
    public int getIdIsIn() {
        return idIsIn;
    }

    public void setIdIsIn(int idIsIn) {
        this.idIsIn = idIsIn;
    }

    @Basic
    @Column(name = "quantity", nullable = false)
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Basic
    @Column(name = "unit", nullable = true, length = 5)
    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IsIn isIn = (IsIn) o;
        return idIsIn == isIn.idIsIn &&
                quantity == isIn.quantity &&
                Objects.equals(unit, isIn.unit);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idIsIn, quantity, unit);
    }

    @ManyToOne
    @JoinColumn(name = "recipe", referencedColumnName = "id_recipe", nullable = false)
    public Recipe getRecipeByRecipe() {
        return recipeByRecipe;
    }

    public void setRecipeByRecipe(Recipe recipeByRecipe) {
        this.recipeByRecipe = recipeByRecipe;
    }

    @ManyToOne
    @JoinColumn(name = "ingredient", referencedColumnName = "id_ingredient", nullable = false)
    public Ingredient getIngredientByIngredient() {
        return ingredientByIngredient;
    }

    public void setIngredientByIngredient(Ingredient ingredientByIngredient) {
        this.ingredientByIngredient = ingredientByIngredient;
    }
}
