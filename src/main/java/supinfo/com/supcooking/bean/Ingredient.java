package supinfo.com.supcooking.bean;

import org.hibernate.annotations.Proxy;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Ingredient {
    private int idIngredient;
    private String name;
    private String description;
    private byte epiced;
    private Byte allergen;

    @Id
    @Column(name = "id_ingredient", nullable = false)
    public int getIdIngredient() {
        return idIngredient;
    }

    public void setIdIngredient(int idIngredient) {
        this.idIngredient = idIngredient;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description", nullable = true, length = -1)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "epiced", nullable = false)
    public byte getEpiced() {
        return epiced;
    }

    public void setEpiced(byte epiced) {
        this.epiced = epiced;
    }

    @Basic
    @Column(name = "allergen", nullable = true)
    public Byte getAllergen() {
        return allergen;
    }

    public void setAllergen(Byte allergen) {
        this.allergen = allergen;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ingredient that = (Ingredient) o;
        return idIngredient == that.idIngredient &&
                epiced == that.epiced &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description) &&
                Objects.equals(allergen, that.allergen);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idIngredient, name, description, epiced, allergen);
    }
}
