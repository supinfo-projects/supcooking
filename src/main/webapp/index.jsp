﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ page import="supinfo.com.supcooking.dao.RecipeDAO"%>
<%@ page import="supinfo.com.supcooking.dao.UserDAO"%>
<%@ page import="supinfo.com.supcooking.bean.Recipe" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<%! private String img_url; private String desc; private String name;
    private Long nu; private long ru; private int recipeId;%>

<%
    RecipeDAO recipeDAO = new RecipeDAO();
    UserDAO u = new UserDAO();
    nu = u.quantity();
    ru = recipeDAO.quantity();
    try {
        Recipe r = recipeDAO.findLast();
        img_url = r.getPicture();
        desc = r.getDescription();
        name = r.getName();
        recipeId = r.getIdRecipe();
    } catch (Exception e) {
        img_url = "images/recipe/default.jpg";
        desc = "No recipe yet";
        name = "404 Recipe not found";
        recipeId = -1;
    } %>
<tag:head name="Supcooking :: Welcome" root_path=""/>
<tag:header logged="false" />
<div class="main">
    <span class="notconnected">You are not log in the application. Please log in or register to enjoy this app.</span>
    <div class="app-pres">
        <b>Sup Cooking</b> is a small application who allow to share cooking recipe and note them.
    </div>
    <div class="recipe">
        <h1>Last Recipe posted</h1>
        <tag:recipe username="none" minified="true" recipe_id="<%=recipeId%>"/>
    </div>
    <div class="stats">
        <div>
            <span class="name">Number of register users</span>
            <span class="number"><%=nu%></span>
        </div>
        <div>
            <span class="name">Number of recipe shared</span>
            <span class="number"><%=ru%></span>
        </div>
    </div>
</div>
</body></html>
