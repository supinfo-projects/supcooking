﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ page import="supinfo.com.supcooking.dao.RecipeDAO"%>
<%@ page import="supinfo.com.supcooking.dao.UserDAO"%>
<%@ page import="supinfo.com.supcooking.bean.Recipe" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="supinfo.com.supcooking.utils.Chopped" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%! private int recipeId; private Long nu; private long ru;
    private String username; private String avatar; private String level;
    private ArrayList<Recipe> recipes; private int i = 0; private int length = 0;
    private List<List<Recipe>> choppedRecipes; private long ruu;
%>

<%
    RecipeDAO recipeDAO = new RecipeDAO();
    UserDAO u = new UserDAO();
    Chopped chopper = new Chopped();
    nu = u.quantity();
    ru = recipeDAO.quantity();
    try {
        recipes = (ArrayList)recipeDAO.findAll();
        length = recipes.size();
        choppedRecipes = chopper.chopped(recipes, 10);
    } catch (Exception e) {
        recipes  = new ArrayList<>();
    }
    try {
        username = (String)session.getAttribute("username");
        ruu = recipeDAO.sharedBy(username);
    } catch (Exception e) {
       username = "Ninja";
       ruu = 999999999;
    }
    try {
        avatar = "../" + (String)session.getAttribute("avatar");
    } catch (Exception e) {
        avatar = "../images/avatars/user.png";
    }
    try {
        level = ((Integer)session.getAttribute("level")).toString();
    } catch (Exception e) {
        level = "1";
    }
%>
<tag:head name="Supcooking :: Welcome" root_path="../"/>
    <tag:header logged="true" username="<%=username%>" avatar="<%=avatar%>" level="<%=level%>"/>
    <div class="main">
        <div class="app-pres">
            <b>Sup Cooking</b> is a small application who allow to share cooking recipe and note them.
        </div>
        <div class="recipes">
            <h1>Recipes</h1>
            <nav-bar>
                <c:forEach var="page" begin="0" end="<%=(length-1)%10%>">
                    <label>
                        <input class="hidden" type="radio" id="page-${page}">
                        <span>${page}</span>
                    </label>
                </c:forEach>
            </nav-bar>
            <c:forEach var="recipeList" items="<%=choppedRecipes%>">
                <div class="block<%=(++i)-1%>">
                    <c:forEach var="recipe" items="${recipeList}">
                        <tag:recipe username="<%=username%>" minified="true" recipe_id="${recipe.idRecipe}" root_path="../"/>
                    </c:forEach>
                </div>
            </c:forEach>
        </div>
        <div class="stats">
            <div>
                <span class="name">
                Number of register users
                </span>
                <span class="number"><%=nu%></span>
            </div>
            <div>
                <span class="name">Number of recipe shared</span>
                <span class="number"><%=ru%></span>
            </div>
            <div>
                <span class="name">Number of recipe you shared</span>
                <span class="number"><%=ruu%></span>
            </div>
        </div>
    </div>
</body></html>
