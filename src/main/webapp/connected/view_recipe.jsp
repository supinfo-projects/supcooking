<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<%! private int recipeId;
    private String username; private String avatar; private String level;
%>

<%
    try {
        recipeId = (Integer)request.getAttribute("id_recipe");
    } catch (Exception e) {
        recipeId = -1;
    }
    try {
        username = (String)session.getAttribute("username");
    } catch (Exception e) {
        username = "Ninja";
    }
    try {
        avatar = "../"+(String)session.getAttribute("avatar");
    } catch (Exception e) {
        avatar = "images/avatars/user.png";
    }
    try {
        level = ((Integer)session.getAttribute("level")).toString();
    } catch (Exception e) {
        level = "1";
    }
%>
<tag:head name="Supcooking :: Recipe" root_path="../"/>
<tag:header logged="false" username="<%=username%>" avatar="<%=avatar%>" level="<%=level%>"/>
<div class="main">
    <tag:recipe minified="false" recipe_id="<%=recipeId%>" username="<%=username%>" root_path="../" />
</div>
</body>
</html>
