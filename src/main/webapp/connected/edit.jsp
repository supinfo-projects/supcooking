<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%! private String email; private String password; private String firstName;
    private String lastName; private String avatar; private int level; private int zipcode = 0;
    private String strLevel; private String username; private Boolean emailExist = false;
%>
<%
    try {
        username = (String) request.getAttribute("username");
    } catch (Exception e){ }
    try {
        email = (String) request.getAttribute("email");
    } catch (Exception e){ }
    try {
        password = (String) request.getAttribute("password");
    } catch (Exception e){ }
    try {
        firstName = (String) request.getAttribute("firstName");
    } catch (Exception e){ }
    try {
        lastName = (String) request.getAttribute("lastName");
    } catch (Exception e){ }
    try {
        avatar = (String) request.getAttribute("email");
    } catch (Exception e){ }
    try {
        level = (Integer) request.getAttribute("level");
    } catch (Exception e){ }
    try {
        zipcode = (Integer) request.getAttribute("zipcode");
    } catch (Exception e){ }
    try {
        strLevel = Integer.toString(level);
    } catch (Exception e) {}
%>
<tag:head name="Supcooking :: Edit <%=username%> profile" root_path="../"/>
<tag:header logged="true" username="<%=username%>" avatar="../<%=avatar%>" level="<%=strLevel%>"/>
<div class="main">

    <h1>Edit profile: <%=username%></h1>
    <form action="Edit" method="post">
        <label>
            <span>Email :</span>
            <c:choose>
                <c:when test="<%=emailExist%>">
                    <script>window.alert('This email is already in use. Please enter another one')</script>"
                    <input class='error' type="email" name='email' size='10'
                           placeholder='Email already in use' required invalid/>
                </c:when>
                <c:otherwise>
                    <input type="email" name="email" max-size="255"
                           value="<%=email%>" required title="Must be well formed email: something@domain.loc"/>"
                </c:otherwise>
            </c:choose>
        </label>
        <label>
            <span>Password :</span>
            <input type="password" name="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).{8,}" value="<%=password%>"
                   title="Must contain at least one number, one uppercase, one lowercase letter, one special in {@, #, $, %, ^, &, +, =], and at least 8 characters." />
        </label>
        <label>
            <span>First Name :</span>
            <input type="text" name="name" value="<%=firstName%>"/>
        </label>
        <label>
            <span>Last Name :</span>
            <input type="text" name="lastname"  value="<%=lastName%>"/>
        </label>
        <label>
            <span>ZipCode :</span>
            <input type="number" size="5" name="zip"  value="<%=zipcode%>"/>
        </label>
        <div>
            <span>Level :</span>
            <tag:stars_rating name="level" rating="<%=strLevel%>" />
        </div>

        <label>
            <span>Avatar url :</span>
            <input type="image" name="avatar" dirname="images/avatars" value="<%=avatar%>"/>
        </label>
        <!-- 	<label>
                <span>Confirm Password :</span>
                <input type="password" name="password2" size="30" />
            </label> -->
        <input type="submit" value="Edit" />
    </form>
    <a href="index.jsp"><button>Back to home</button></a>
</div>
</body>
</html>
