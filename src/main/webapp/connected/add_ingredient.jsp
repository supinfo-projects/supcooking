<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="supinfo.com.supcooking.dao.IngredientDAO" %>
<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%!  private int nIng = 0; private String recipeName = "";
    private String username; private String avatar; private String level;
    private IngredientDAO ingredientDAO = new IngredientDAO();
    private ArrayList<String> ingredientNames = (ArrayList<String>)ingredientDAO.findNames();
%>
<%
    try {
        username = (String)session.getAttribute("username");
    } catch (Exception e) {
        username = "Ninja";
    }
    try {
        avatar = (String)session.getAttribute("avatar");
    } catch (Exception e) {
        avatar = "images/avatars/user.png";
    }
    try {
        level = ((Integer)session.getAttribute("level")).toString();
    } catch (Exception e) {
        level = "1";
    }
    try {
        nIng = Integer.parseInt(request.getParameter("n_ingredient"));
    } catch (Exception e) {
        out.print("Error retrieving ingredient number from request");
    };
    try {
        recipeName = request.getParameter("name");
    } catch (Exception e) { };
%>
<tag:head name="Supcooking :: Add Ingredients for <%=recipeName%>" root_path = "../" />
<tag:header logged="true" username="<%=username%>" avatar="../<%=avatar%>" level="<%=level%>"/>
<div class="main">
    <h1>Add ingredient to recipe: <%=recipeName%></h1>
    <form action="AddRecipe" method="post">

        <input class="hidden" type="number" name="nb_ingredient" value="<%=nIng%>"/>

        <c:forEach var="i" begin="0" end="<%=nIng-1%>">
            <select name="ingredient_${i}" required>
                <c:forEach items="<%=ingredientNames%>" var="name">
                    <option value="${name}">${name}</option>
                </c:forEach>
                <option value="new_ingredient">New ingredient: </option>
            </select>
            <div class="new_ingredient">
                <input type="text" placeholder="New ingredient name" name="new_ingredient_name_${i}"
                        pattern="[a-zA-Z][a-zA-Z ]*[a-z]" title="User must be alphabetic with space."/>
                <input type="text" placeholder="New ingredient description" name="new_ingredient_desc_${i}">
                <input type="checkbox" name="new_ingredient_${i}_is_epiced" value="true" ><span>Epiced</span>
                <input type="checkbox" name="new_ingredient_${i}_is_allergen" value="true" ><span>Allergen</span>
            </div>
            <label>
                <span>Quantity: </span><input name="quantity_${i}" type="number" required>
            </label>
            <label>
                <span>Unit: </span><input name="unit_${i}" type="text" value="unit">
            </label>
        </c:forEach>
        <input type="submit" value="Complete recipe" />
    </form>
    <a href="index.jsp"><button>Back to home</button></a>
</div>
</body>
</html>
