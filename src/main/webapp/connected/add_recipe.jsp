<%@ page import="supinfo.com.supcooking.dao.CategoryDAO" %>
<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%!
    private CategoryDAO categoryDAO = new CategoryDAO();
    private ArrayList<String> catNames = (ArrayList<String>)categoryDAO.findNames();
    private String username; private String avatar; private String level;
%>

<%
    try {
        username = (String)session.getAttribute("username");
    } catch (Exception e) {
        username = "Ninja";
    }
    try {
        avatar = (String)session.getAttribute("avatar");
    } catch (Exception e) {
        avatar = "images/avatars/user.png";
    }
    try {
        level = ((Integer)session.getAttribute("level")).toString();
    } catch (Exception e) {
        level = "1";
    }
%>
<tag:head name="Supcooking :: Add recipe" root_path="../"/>
<tag:header logged="true" username="<%=username%>" avatar="../<%=avatar%>" level="<%=level%>"/>
<div class="main">
    <h1>Create recipe</h1>
    <form action="AddIngredientRecipe" method="post" enctype="multipart/form-data">
        <label>
            <span>Name :</span>
            <input type="text" name="name" required/>
        </label>
        <label>
            <span>Category: </span>
            <select required name="category">
                <c:forEach items="<%=catNames%>" var="name">
                    <option value="${name}">${name}</<option>
                </c:forEach>
                <option value="new_category">
                   new_category:
                </option>
            </select>
            <input type="text" name="new_category" placeholder="New category name."/>
        </label>
        <label>
            <span>Description</span>
            <textarea name="description" id="description" cols="30" rows="10"></textarea>
        </label>
        <label>
            <span>Preparation time</span>
            <input type="number" name="prep" required> min
        </label>
        <label>
            <span>Cooking time</span>
            <input type="number" name="cooking" required> min
        </label>
        <label>
            <span>Number of ingredient :</span>
            <input type="number" size="5" name="n_ingredient" required/>
        </label>
        <label>
            <span>Recipe image :</span>
            <input type="file" accept="image/png, image/jpeg, image/giff" name="avatar" />
        </label>
        <div>
            <span> Hardness</span><br/>
            <tag:stars_rating name="hardness" rating="0"/>
        </div>
        <!-- 	<label>
                <span>Confirm Password :</span>
                <input type="password" name="password2" size="30" />
            </label> -->
        <input type="submit" value="Add ingredient" />
    </form>
    <a href="index.jsp"><button>Back to home</button></a>
</div>
</body>
</html>
