<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%!
    private Boolean emailTest = false; private Boolean userTest = false;
%>
<tag:head name="Supcooking :: 500 ERROR"/>
<tag:header logged="false" />
<div class="main">
    <h1>Registration Form</h1>
    <form action="Register" method="post">
        <label>
            <span>Email :</span>
                <c:choose>
                    <c:when test="<%=emailTest%>">
                        <script>window.alert('This email is already in use. Please enter another one')</script>
                        <input class='error' type='email' name='email' size='10' placeholder='Email already in use' required
                               invalid pattern="[a-zA-Z].*@([a-zA-Z1-9_-]+[.]{1})*[a-zA-Z]{2,6}" title="Must be well formed email: something@domain.loc"/>
                    </c:when>
                    <c:otherwise><input type='email' name='email' pattern="[a-zA-Z].*@([a-zA-Z1-9_-]+[.]{1})*[a-zA-Z]{2,6}"
                                        required title="Must be well formed email: something@domain.loc"/>
                    </c:otherwise>
                </c:choose>
        </label>
        <label>
            <span>Username:</span>
            <c:choose>
                <c:when test="<%=userTest%>">
                    <script>window.alert('This username is already in use. Please enter another one')</script>
                    <input class='error' type='str' name='username' size='6' placeholder='Username already in use' required invalid/>
                </c:when>
                <c:otherwise>
                    <input type="text" name="username" pattern="[a-zA-Z]+[a-zA-Z1-9_@%]*[a-zA-Z1-9]"
                           title="Username can only contain alpha numerics, _, @, % and must start by a character and end by a character or a digit" size='10' required />
                </c:otherwise>
            </c:choose>
        </label>
        <label>
            <span>Password :</span>
            <input type="password" name="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).{8,}"
                   title="Must contain at least one number, one uppercase, one lowercase letter, one special in {@, #, $, %, ^, &, +, =}, and at least 8 characters." />
        </label>
        <label>
            <span>First Name :</span>
            <input type="text" name="name"/>
        </label>
        <label>
            <span>Last Name :</span>
            <input type="text" name="lastname" />
        </label>
        <label>
            <span>ZipCode :</span>
            <input type="number" size="5" name="zip" />
        </label>
        <div>
            <span>Level :</span>
            <tag:stars_rating rating="0" name="level"/>
        </div>

        <label>
            <span>Avatar url :</span>
            <input type="file" name="avatar" accept="image/gif,image/png,image/jpg"/>
        </label>
        <!-- 	<label>
                <span>Confirm Password :</span>
                <input type="password" name="password2" size="30" />
            </label> -->
        <input type="submit" value="Register" />
    </form>
    <a href="index.jsp"><button>Back to home</button></a>
</div>
</body>
</html>
