<%@ tag import="supinfo.com.supcooking.bean.Ingredient" %>
<%@tag description="recipe printer" pageEncoding="UTF-8"%>

<%@attribute name="name" required="true" %>
<%@attribute name="rating" required="true" %>

<%! private String check(String i, String v) {
    if (i.equals(v)) {
        return " checked";
    }
    return "";
}%>
<fieldset class="rating">
    <input type="radio" id="${name}-star5" name="${name}" value="10"<%=check("10",rating)%>/>
    <label class = "full" for="${name}-star5" title="Awesome"></label>

    <input type="radio" id="${name}-star4half" name="${name}" value="9"<%=check("9",rating)%>/>
    <label class="half" for="${name}-star4half" title="Pretty good"></label>

    <input type="radio" id="${name}-star4" name="${name}" value="8"<%=check("8",rating)%>/>
    <label class = "full" for="${name}-star4" title="Pretty good"></label>

    <input type="radio" id="${name}-star3half" name="${name}" value="7"<%=check("7",rating)%>/>
    <label class="half" for="${name}-star3half" title="Meh"></label>

    <input type="radio" id="${name}-star3" name="${name}" value="6"<%=check("6",rating)%>/>
    <label class = "full" for="${name}-star3" title="Meh"></label>

    <input type="radio" id="${name}-star2half" name="${name}" value="5"<%=check("5",rating)%>/>
    <label class="half" for="${name}-star2half" title="Kinda bad"></label>

    <input type="radio" id="${name}-star2" name="${name}" value="4"<%=check("4",rating)%>/>
    <label class = "full" for="${name}-star2" title="Kinda bad"></label>

    <input type="radio" id="${name}-star1half" name="${name}" value="3"<%=check("3",rating)%>/>
    <label class="half" for="${name}-star1half" title="Bad"></label>

    <input type="radio" id="${name}-star1" name="${name}" value="2"<%=check("2",rating)%>/>
    <label class = "full" for="${name}-star1" title="Sucks big time"></label>

    <input type="radio" id="${name}-starhalf" name="${name}" value="1"<%=check("1",rating)%>/>
    <label class="half" for="${name}-starhalf" title="Sucks big time"></label>
</fieldset>
