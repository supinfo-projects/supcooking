<%@ tag description="head printer" pageEncoding="UTF-8"%>
<%@attribute name="name" required="true" %>
<%@attribute name="root_path" required="false" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>${name}</title>
        <link rel='icon' href='${root_path}images/favicon.ico' />
        <link rel='icon' type='image/gif' href='${root_path}images/favicon.gif' />
        <!-- loading CSS -->
        <link href='${root_path}css/fontawesome-all.min.css' rel='stylesheet'>
        <link href='${root_path}css/style.css' rel='stylesheet'>
    </head>
    <body>