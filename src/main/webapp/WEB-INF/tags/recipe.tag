<%@ tag import="supinfo.com.supcooking.bean.Ingredient" %>
<%@ tag import="supinfo.com.supcooking.dao.IngredientDAO" %>
<%@ tag import="supinfo.com.supcooking.dao.IsInDAO" %>
<%@ tag import="java.util.ArrayList" %>
<%@ tag import="supinfo.com.supcooking.dao.RecipeDAO" %>
<%@ tag import="supinfo.com.supcooking.bean.Recipe" %>
<%@ tag import="supinfo.com.supcooking.dao.RankDAO" %>
<%@ tag description="recipe printer" pageEncoding="UTF-8" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@attribute name="minified" required="true" %>
<%@attribute name="recipe_id" required="true" type="java.lang.Integer" %>
<%@attribute name="root_path" %>
<%@attribute name="username" required="true" %>
<%!
    private RecipeDAO recipeDAO = new RecipeDAO();
    private RankDAO rankDAO = new RankDAO();
    private IsInDAO isInDAO = new IsInDAO();
    private ArrayList<Ingredient> ingredients;
    private int rank;
    private String img_url;
    private String desc;
    private String name;
    private Recipe recipe;
    private int hardness;
    private int cookingTime;
    private int prepTime;
    private boolean complete;
    private boolean owned;
    private boolean complete_owned;
    private boolean logedIn;
%>
<%
    complete = false;
    owned = false;
    complete_owned=false;
    if (recipe_id != -1) {
        recipe = recipeDAO.get(recipe_id);
        try {
            ingredients = (ArrayList<Ingredient>) isInDAO.getIngredients(recipe);
        } catch (Exception e) {
            ingredients = new ArrayList<>();
        }
        try {
            if (username.equals("none")) {
                rank = 0;
                owned = false;
                logedIn = false;
            } else {
                logedIn = true;
                rank = rankDAO.getRank(recipe, username);
                owned = username.equals(recipe.getUserByPublishedBy().getUsername());
            }
        } catch (Exception e) {
            rank = 0;
            owned = false;
            logedIn = false;
        }
        try {
            complete = minified.equals("false");
        } catch (Exception e) {
            complete = true;
        }
        try {
            complete_owned = complete && owned;
        } catch (Exception e) {
            complete_owned = false;
        }
        name = recipe.getName();
        img_url = recipe.getPicture();
        desc = recipe.getDescription();
        session.setAttribute("recipe", recipe);
        try {
            hardness = recipe.getHardness();
        } catch (Exception e){
            hardness = 0;
        }
        try {
            cookingTime = recipe.getCookingTime();
        } catch (Exception e) {
            cookingTime = 20;
        }
        try {
            prepTime = recipe.getPreparationTime();
        } catch (Exception e) {
            prepTime = 20;
        }
    }
    if (recipe_id < 1) {
        name = "404 Not Found";
        img_url = "images/recipe/default.jpg";
        desc = "No recipe yet";
    }
    if (root_path == null || root_path.equals("null")) {
        root_path = "";
    }
%>
<label style="cursor: zoom-in" for="recipe_<%=recipe_id%>">
    <div class="recipe">
        <header>
            <h4><%=name%></h4>
            <c:if test="<%=complete_owned%>">
                <div class="actions">
                    <form method="post" action="DeleteRecipe"><input type="submit" value="Delete"></form>
                    <form method="post" action="EditRecipe"><input type="submit" value="Edit"></form>
                </div>
                </c:if>
        </header>
        <div class="desc">
            <img src="<%=root_path%><%=img_url%>"/>
            <span><%=desc%></span>
        </div>
        <c:if test="<%=complete%>">
            <div class="details">
                <div class="cooking_information">
                    <span>Cooking time <%=cookingTime%> minutes</span>
                    <span>Preparation time <%=prepTime%> minutes</span>
                    <span>Hardness: <tag:stars_rating name="hardness" rating="<%=Integer.toString(hardness)%>"/></span>
                </div>
                <div class="ingredient-list">
                    <c:forEach items="<%=ingredients%>" var="ing">
                        <h1><c:out value="${ing.name}"/></h1>
                        <div class="ingredient">
                            <span class="name">${ing.name}</span>
                            <span class="description">${ing.description}</span>
                            <div class="dangers">
                                <c:if test="${ing.allergen.equals(1)}">
                                    <i class="fas fa-allergies"></i>
                                </c:if>
                                <c:if test="${ing.epiced.equals(1)}">
                                    <i class="fab fa-hotjar"></i>
                                </c:if>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </c:if>
        <c:choose>
            <c:when test="<%=owned%>">
                <div class="rating">
                    <tag:stars_rating name="recipe_rating" rating="<%=Integer.toString(rank)%>"/>
                </div>
            </c:when>
            <c:when test="<%=complete%>">
                <form action="Rank">
                    <div class="rating">
                        <tag:stars_rating name="recipe_rating" rating="<%=Integer.toString(rank)%>"/>
                    </div>
                </form>
            </c:when>
        </c:choose>
    </div>
</label>
<c:if test="<%=logedIn%>">
    <form method="post" action="ViewRecipe">
        <input type="text" class="hidden" value="<%=recipe_id%>" name="id_recipe">
        <input type="submit" class="hidden" id="recipe_<%=recipe_id%>">
    </form>
</c:if>