<%@ tag import="supinfo.com.supcooking.bean.Ingredient" %>
<%@ tag description="header printer" pageEncoding="UTF-8"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@attribute name="logged" required="true" %>
<%@attribute name="username" required="false" %>
<%@attribute name="level" required="false" %>
<%@attribute name="avatar" required="false" %>
<%!
    private boolean log_in; private String outAvatar;
%>
<%
    outAvatar = avatar;
    if (username == null || username.equals("")) {
        username = "Login";
        outAvatar = "images/avatars/anonymous.png";
    }
    log_in = logged.equals("true");
%>
<header>
    <div class="banner">
        <div class="logo"></div>
        <div class="titles">
            <h1>Sup Cooking</h1>
            <h2>Le meilleur du partage.</h2>
        </div>
        <div class="user">
            <label for="user_action" style="cursor: pointer;">
                <div class="avatar">
                    <img src="<%= outAvatar %>">
                </div>
                <div class="user_data">
                    <span class="user"><%=username%></span>
                    <c:choose>
                        <c:when test="<%=log_in%>">
                            <div class="level untargetable">
                                <tag:stars_rating name="unmached" rating="<%=level%>"/>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <form class="login" method="post" action="Login">
                                <input name="id" type="text" required placeholder="Username"/>
                                <input name="password" type="password" required placeholder="Password"/>
                                <input type="submit" class="hidden" id="user_action"/>
                            </form>
                        </c:otherwise>
                    </c:choose>
                </div>
            </label>
            <div class="forms">
            <c:choose>
                <c:when test="<%=log_in%>">
                    <form action="SetEdit" class="hidden">
                        <input type="submit" class="hidden" id="user_action">
                    </form>
                    <form class="logout" action="Logout">
                        <input type="submit" value="Log Out">
                    </form>
                </c:when>
                <c:otherwise>
                    <button class="register">
                        <a href="register.jsp">Register</a>
                    </button>
                </c:otherwise>
            </c:choose>
            </div>
        </div>
    </div>
    <c:if test="<%=log_in%>">
        <nav>*
            <a href="index.jsp">Home</a>
            <a href="add_recipe.jsp">Add Recipe</a>
        </nav>
    </c:if>
</header>