FROM tomcat:8
EXPOSE 8080
COPY target/supcooking.war /usr/local/tomcat/webapps/supcooking.war
COPY tomcat-users.xml /usr/local/tomcat/conf/tomcat-users.xml