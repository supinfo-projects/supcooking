# Sup Cooking

Maven based JEE project

*Note: 12*

## Requierements

- Java 8 or greater (jre + jdk)
- maven 3

## Opt dependencies

- Docker
- Docker-compose

## Running the project

- `docker-compose up -d` to launch the default database containers
- `maven build && maven org.apache.tomcat.maven:tomcat7-maven-plugin:2.2:run`

## Class diagrams

Global schema

![Global application dependencie diagram][global-uml]

### Bean package

![Bean package][bean-uml]

### Database

![Database schema][db-uml]

### DAO package

![Dao package][dao-uml]

## Technologies

### JSP/JSTL

Usage of standar `JSP` with `tag` and `JSTL` core library to have clean view templating.

### Maven

Package the application using `maven` to have the easiest sharing as possible. Every dependencies version is fixed so it should work for any one.

### Hibernate/JPA Persistence/MariaDB

Data are managed through a `MariaDB` SQL database, and the combo `Hibernate/JPA` using `MySQL` dialect and database connection as `MariaDB` and `MySQL` are quite the same when it comes to query.

### SASS/Compass/Fontawesome

Styling through `SASS` using `Compass` pre-existing meta and `Fontawesome` icon based to manage application styling.

[db-uml]: db/db_shema.png
[bean-uml]: class_diagramms/bean.png
[dao-uml]: class_diagramms/dao.png
[global-uml]: class_diagramms/global.png
